import React, { useState } from 'react';
import Dice from '../../component/Dice';

const DicePrincipal = () => {
  return (
    <div className='container'>
      <h1 className='title'>Jeux de Dés</h1>
      <Dice/>
    </div>
  );
};

export default DicePrincipal;