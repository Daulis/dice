import React, { useState, useEffect } from 'react';

const Dice = () => {
  const targetScore = 50;

  const [scores, setScores] = useState({ joueur1: 0, joueur2: 0 });
  const [currentPlayer, setCurrentPlayer] = useState('joueur1');
  const [throuingCount, setThrouingCount] = useState(0);
  const [result, setResult] = useState('Jeux en cours');
  const [diceState, setDiceState] = useState({ dice1: 1, dice2: 1 });

  useEffect(() => {
    setDiceState({ dice1: 1, dice2: 1 });
  }, []);

  const switchPlayer = () => {
    setCurrentPlayer(currentPlayer === 'joueur1' ? 'joueur2' : 'joueur1');
  };

  const updateScore = (dice_1, dice_2) => {
    setScores((prevScores) => ({
      ...prevScores,
      [currentPlayer]: prevScores[currentPlayer] + dice_1 + dice_2,
    }));
    setThrouingCount((prevThrouingCount) => prevThrouingCount + 1);

    if (scores[currentPlayer] + dice_1 + dice_2 >= targetScore) {
      setResult(`${currentPlayer} a gagné après ${throuingCount} lancer de dés!`);
    } else {
      switchPlayer();
    }
  };

  /**
   * On réinitialise l'état initiale du dés lorsqu'on clique sur le boutton
   */
  const resetScore = () => {
    setScores({ joueur1: 0, joueur2: 0 });
    setThrouingCount(0);
    setCurrentPlayer(Math.random() < 0.5 ? 'joueur1' : 'joueur2');
    setResult('Jeux en cours');
    setDiceState({ dice1: 1, dice2: 1 });
  };

  const play = () => {
    const firstDice = Math.floor(Math.random() * 6) + 1;
    const secondDice = Math.floor(Math.random() * 6) + 1;
    setDiceState({ dice1: firstDice, dice2: secondDice });
    updateScore(firstDice, secondDice);
    switchPlayer();
  };
const isButtonDisabled = scores[currentPlayer] >= targetScore;
  return (
    <div className='dice'>
      <div className='section'>
        <p className='current_player'>Tour du: <span className='tour'>{currentPlayer}</span></p>
        <img src={`images/Dés_${diceState.dice1}.jpg`} alt={`Dice ${diceState.dice1}`} />
        <img src={`images/Dés_${diceState.dice2}.jpg`} alt={`Dice ${diceState.dice2}`} />
        <p>Total: <span className='total'>{diceState.dice1 + diceState.dice2}</span> </p>
        <button onClick={play} disabled={isButtonDisabled}>Lancer le dé</button>
      </div>
      <div className='section_scores'>
        <p className='joueur'>Score Joueur 1: <span>{scores.joueur1}</span></p>
        <p className='joueur'>Score Joueur 2: <span>{scores.joueur2}</span></p>
      </div>
      <p className='result'>{result}</p>
      <button onClick={resetScore}>Réinitialiser le score</button>
    </div>
  );
};

export default Dice;
