import DicePrincipal from "./pages/dice/dice";

function App() {
  return (
    <div className="App">
      <DicePrincipal/>
    </div>
  );
}

export default App;
